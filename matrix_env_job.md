# Tableau d'environnements et de job

| Environnement | quality | plan | cost | apply |
|--------------|----------|------|------|-------|
| dev          |    V   |    V   |    V   |   V  |
| staging      |    X   |    V   |    X   |   V  |
| prod         |    X   |    V   |    X   |   V  |


